﻿/*
	Tyler Chapman and Alexander West
	EGP-405-01
	Final Project
	Due December 13

	I certify that this work is entirely my own. The assessor of this
	project may reproduce this project and provide copies to other academic staff,
	and/or communicate a copy of this project to a plagiarism-checking service,
	which may retain a copy of the project on its database
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

// need this!
using System.Runtime.InteropServices;

public class GameManager : MonoBehaviour
{
    [DllImport("egp-net-plugin-Unity")]
    static extern int sendMessage(byte[] message, float unityTime);

    public int maxMessages = 25;
    public string username = "";
    public InputField chatBox;

    public GameObject chatSystem;

    public Color playerMessage, info;

    [SerializeField]
    GameObject chatPanel, textObject;

    [SerializeField]
    List<Message> messageList = new List<Message>();

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if(Input.GetKeyDown(KeyCode.BackQuote))
        {
            // if it is already active then set it inactive, else, set it active
            if(chatSystem.activeSelf)
            {
               
                chatSystem.gameObject.SetActive(false);
            }
            else
            {
                sendMessage(Encoding.UTF8.GetBytes("CHAT LOG OPENED"), Time.realtimeSinceStartup);
                chatSystem.gameObject.SetActive(true);
            }
        }

        // check if the input box is enabled
        if (chatSystem.activeSelf)
        {
            if (chatBox.text != "")
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    // message is sent here, send it out here
                    sendMessage(Encoding.UTF8.GetBytes(chatBox.text), Time.realtimeSinceStartup);
                    SendMessageToChat(username + ": " + chatBox.text, Message.MessageType.playerMessage);
                    chatBox.text = "";
                }
            }
            else
            {
                if (!chatBox.isFocused && Input.GetKeyDown(KeyCode.Return))
                {
                    chatBox.ActivateInputField();
                }
            }

            // check if you are in the text box, if not, then you can hit the space key event
            if (!chatBox.isFocused)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    SendMessageToChat("You pressed space key!", Message.MessageType.info);
                }
                // used to track what key is down when you are not in the text field
                /*if (Input.anyKeyDown)
                {
                    SendMessageToChat("You pressed " + Input.inputString + " key", Message.MessageType.info);
                }*/
            }
        }
    }

    public void SendMessageToChat(string text, Message.MessageType messageType)
    {
        if(messageList.Count >= maxMessages)
        {
            Destroy(messageList[0].textObject.gameObject);
            messageList.Remove(messageList[0]);
        }
        Message newMessage = new Message();

        newMessage.text = text;

        GameObject newText = Instantiate(textObject, chatPanel.transform);

        newMessage.textObject = newText.GetComponent<Text>();

        newMessage.textObject.text = newMessage.text;
        newMessage.textObject.color = MessageTypeColor(messageType);

        messageList.Add(newMessage);
    }
    Color MessageTypeColor(Message.MessageType messageType)
    {
        Color color = info;

        switch(messageType)
        {
            case Message.MessageType.playerMessage:
                color = playerMessage;
                break;
        }

        return color;
    }
   
}

[System.Serializable]
public class Message
{
    public string text;
    public Text textObject;
    public MessageType messageType;

    public enum MessageType
    {
        playerMessage,
        info
    }
}