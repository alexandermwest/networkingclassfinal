﻿/*
	Tyler Chapman and Alexander West
	EGP-405-01
	Final Project
	Due December 13

	I certify that this work is entirely my own. The assessor of this
	project may reproduce this project and provide copies to other academic staff,
	and/or communicate a copy of this project to a plagiarism-checking service,
	which may retain a copy of the project on its database
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;

public class TestScript : MonoBehaviour {

    public int testInt;
    public string testString;
    public float testFloat;
    public double testDouble;
    public char[] testArray;
    public char testChar;
    public List<char> testList;

	// Use this for initialization
	void Start ()
    {

	}

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(new string(testArray));
        if (Input.GetKeyDown(KeyCode.Return))
        {
            testInt++;
        }
	}
}
