#ifndef TEMPLATE_SERIALIZABLE_DATA_H
#define TEMPLATE_SERIALIZABLE_DATA_H

#include "egpNetSerializableData.h"

template<typename T>
class TemplateSerializableData : public egpSerializableData
{
	public:
		TemplateSerializableData(T data = nullptr);
		~TemplateSerializableData();

		int Serialize(RakNet::BitStream * bs);
		int Deserialize(RakNet::BitStream * bs);

		TemplateSerializableData(const TemplateSerializableData &) = delete;
		TemplateSerializableData &operator =(const TemplateSerializableData &rhs) = delete;
		TemplateSerializableData(TemplateSerializableData &&) = delete;
		TemplateSerializableData &operator =(TemplateSerializableData &&) = delete;
};

#endif

template<typename T>
inline TemplateSerializableData<T>::TemplateSerializableData(T data)
{
}

template<typename T>
inline TemplateSerializableData<T>::~TemplateSerializableData()
{
}

template<typename T>
inline int TemplateSerializableData<T>::Serialize(RakNet::BitStream * bs)
{
	return 0;
}

template<typename T>
inline int TemplateSerializableData<T>::Deserialize(RakNet::BitStream * bs)
{
	return 0;
}
