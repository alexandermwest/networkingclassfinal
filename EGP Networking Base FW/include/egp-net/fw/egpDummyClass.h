/*
	EGP Networking Framework
	Dan Buckstein
	October 2018

	Dummy class interface.
*/

#ifndef _EGP_DUMMYCLASS_H_
#define _EGP_DUMMYCLASS_H_


class egpDummyClass
{
	int data;

public:
	void setData(int newData = 0);
	int getData() const;
};


#endif	// !_EGP_DUMMYCLASS_H_