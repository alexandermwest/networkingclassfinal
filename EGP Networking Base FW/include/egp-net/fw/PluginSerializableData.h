#ifndef PLUGIN_SERIALIZABLE_DATA_H
#define PLUGIN_SERIALIZABLE_DATA_H

#include "egp-net/fw/egpNetSerializableData.h"

#include <string>

template <class T>
class PluginSerializableData : public egpSerializableData
{
	private:
		unsigned int dataSize;

		unsigned int varNameSize;

		unsigned int ipAddressSize = 16;

	public:
		PluginSerializableData(T data, unsigned int size, unsigned int nameSize, char * name, char * ip);
		virtual ~PluginSerializableData();

		virtual int Serialize(RakNet::BitStream *bs) const;

		virtual int Deserialize(RakNet::BitStream *bs);

		PluginSerializableData(const PluginSerializableData&) = delete;
		PluginSerializableData &operator  =(const PluginSerializableData& rhs) = delete;
		PluginSerializableData(PluginSerializableData&&) = delete;
		PluginSerializableData &operator = (PluginSerializableData&&) = delete;

		T mData;

		char * varName;

		char * ipAddress;
};

#endif

template<typename T>
inline PluginSerializableData<T>::PluginSerializableData(T data, unsigned int size, unsigned int nameSize, char * name, char * ip)
	:mData(data), dataSize(size), varNameSize(nameSize), varName(name), ipAddress(ip)
{

}

template<typename T>
inline PluginSerializableData<T>::~PluginSerializableData()
{
	delete[] varName;
	delete[] ipAddress;
}

template<typename T>
int PluginSerializableData<T>::Serialize(RakNet::BitStream * bs) const
{
	if (bs)
	{
		unsigned int totalSize = 0;

		std::string str = typeid(T).name();

		bs->Write(sizeof(char) * str.length());
		bs->Write(typeid(T).name());

		bs->Write(typeid(T));

		totalSize += sizeof(mData);
		bs->Write(mData);	

		totalSize += sizeof(varNameSize);
		bs->Write(varNameSize);

		if (varNameSize)
		{
			totalSize += varNameSize;
			bs->Write(varName, varNameSize);
		}

		totalSize += sizeof(ipAddressSize);
		bs->Write(ipAddressSize);

		if (ipAddressSize)
		{
			totalSize += ipAddressSize;
			bs->Write(ipAddress, ipAddressSize);
		}


		return totalSize;
	}
	return 0;
}

template<typename T>
int PluginSerializableData<T>::Deserialize(RakNet::BitStream * bs)
{
	if (bs)
	{
		unsigned int totalSize = 0;

		totalSize += sizeof(mData);
		bs->Read(mData);
		
		unsigned int tmpSz;
		totalSize += sizeof(tmpSz);
		bs->Read(tmpSz);

		if (varNameSize != tmpSz)
		{
			delete[] varName;
			varNameSize = tmpSz;
			varName = tmpSz ? new char[tmpSz] : 0;
		}

		if (varNameSize)
		{
			totalSize += varNameSize;
			bs->Read(varName, varNameSize);
		}

		bs->Read(tmpSz);

		if (ipAddressSize != tmpSz)
		{
			delete[] ipAddress;
			ipAddressSize = tmpSz;
			ipAddress = tmpSz ? new char[tmpSz] : 0;
		}

		if (ipAddressSize)
		{
			totalSize += ipAddressSize;
			bs->Read(ipAddress, ipAddressSize);
		}
		return totalSize;
	}
	return 0;
}
