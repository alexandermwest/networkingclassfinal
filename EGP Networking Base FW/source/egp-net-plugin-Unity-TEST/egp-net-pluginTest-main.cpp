/*
	EGP Networking: Plugin Test
	Dan Buckstein
	October 2018

	Developer's test application for plugin for Unity.
*/

// include the plugin interface file to take the "easy route"
//#include "../egp-net-plugin-Unity/egp-net-plugin.h"


// macro for name of plugin
#define PLUGIN_NAME "egp-net-plugin-Unity"


#ifdef _EGP_NET_PLUGIN_H_
// included plugin interface
// link static version of plugin
#pragma comment(lib,PLUGIN_NAME".lib")
#else
// no knowledge of functions in plugin
// the goal of this project is to "fake" what the plugin should do
// therefore, we need to load it and consume it like Unity will
#include <Windows.h>
#endif	// _EGP_NET_PLUGIN_H_

	


int main(int const argc, char const *const *const argv)
{
#ifndef _EGP_NET_PLUGIN_H_
	// consume library
	HMODULE library = LoadLibrary(PLUGIN_NAME".dll");
	if (library)
	{
		// load and test functions from library
		
		// defining the library with a typdef
		typedef int(*retInt_paramInt)(int);
		//typdef int(*retInt_paramVoid)();

		typedef int(*retInt_paramCharPtr)(char*);

		retInt_paramCharPtr startupPlugin = (retInt_paramCharPtr)GetProcAddress(library, "startupPlugin");

		

		// need to cast the result to what it is looking for
		// this is declaring the function foo it is looking for
		//retInt_paramInt foo =
		//	(retInt_paramInt)GetProcAddress(library, "foo");

		
#endif	// !_EGP_NET_PLUGIN_H_

		//startup();

		//int b = foo(4);
		startupPlugin("216.93.149.85");

		//shutdown();

#ifndef _EGP_NET_PLUGIN_H_
		// release library
		FreeLibrary(library);
	}
#endif	// !_EGP_NET_PLUGIN_H_
}
