/*
	EGP Networking Framework
	Dan Buckstein
	October 2018

	Dummy class implementation.
*/

#include "..\..\..\..\include\egp-net\fw\egpDummyClass.h"

void egpDummyClass::setData(int newData)
{
	this->data = newData;
}

int egpDummyClass::getData() const
{
	return this->data;
}
