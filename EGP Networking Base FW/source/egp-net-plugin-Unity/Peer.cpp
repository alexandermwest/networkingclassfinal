/*
	Tyler Chapman and Alexander West
	EGP-405-01
	Final Project
	Due December 13

	I certify that this work is entirely my own. The assessor of this
	project may reproduce this project and provide copies to other academic staff,
	and/or communicate a copy of this project to a plagiarism-checking service,
	which may retain a copy of the project on its database
*/

#include "Peer.h"
#include "RakNet/RakPeerInterface.h"

Peer::Peer()
	:egpNetPeerManager()
{
	mShouldExit = false;
}

Peer::~Peer()
{

}

int Peer::HandleNetworking() const
{
	if (!egpNetPeerManager::HandleNetworking())
	{

	}
	return 0;
}

int Peer::ProcessPacket(const RakNet::Packet * const packet, const unsigned int packetIndex) const
{
	switch (packet->data[0])
	{
		// specific packet logic
		case ID_DISCONNECTION_NOTIFICATION:
		{
			Peer * ptr = const_cast<Peer*>(this);
			ptr->mShouldExit = true;
			break;
		}
		case ID_CONNECTION_LOST:
		{
			Peer * ptr = const_cast<Peer*>(this);
			ptr->mShouldExit = true;
			break;
		}
		default:
		{
			break;
		}
	}

	return 0;
}
