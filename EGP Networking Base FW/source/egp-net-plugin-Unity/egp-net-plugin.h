/*
	EGP Networking: Plugin Interface/Wrapper
	Dan Buckstein
	October 2018

	Main interface for plugin. Defines symbols to be exposed to whatever 
		application consumes the plugin. Targeted for Unity but should also 
		be accessible by custom executables (e.g. test app).
*/

// Edited by Tyler Chapman and Alexander West with permission from Dan Buckstein

#ifndef _EGP_NET_PLUGIN_H_
#define _EGP_NET_PLUGIN_H_

#include "egp-net-plugin-config.h"

#include <fstream>
#include <string>
#include <Peer.h>

Peer * localPeer;

int mConnected = 0;

// this include is needed as the function is used in two places
// this is fine in C++ but not C as the namespace can only be
// used once in C. This the ifdef extern fixes this
// This means that there can be no c++ functionality after this declare
// (No classes)
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

	DLLExport
		int startupPlugin(char* ipAdress);

	DLLExport
		int shutdownPlugin();

	DLLExport int readDataInt(int data, char * varName, float unityTime);
	DLLExport void readDataFloat(float data, char * varName, float unityTime);
	DLLExport void readDataDouble(double data, char * varName, float unityTime);
	DLLExport void readDataChar(char data, char * varName, float unityTime);
	DLLExport void readDataCharP(char * data, char * varName, float unityTime);
	DLLExport void sendMessage(char * message, float unityTime);
	DLLExport bool shouldQuit();

#ifdef __cplusplus
}
#endif

#endif	// !_EGP_NET_PLUGIN_H_