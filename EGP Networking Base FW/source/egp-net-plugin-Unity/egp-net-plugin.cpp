/*
	EGP Networking: Plugin Interface/Wrapper
	Dan Buckstein
	October 2018

	Main implementation of Unity plugin wrapper.
*/

// Edited by Tyler Chapman and Alexander West with permission from Dan Buckstein

#include "egp-net-plugin.h"
#include <iomanip>
#include <sstream>

int startupPlugin(char* ipAdress)
{
	// call connect here
	// successfully read in IP
	if (!mConnected)
	{
		localPeer = new Peer();

		localPeer->StartupNetworking(1, 0, 0);
		mConnected = localPeer->Connect(ipAdress, 60000);
		Sleep(1000);
	}
	return mConnected;
}

int shutdownPlugin()
{
	int shutdown = localPeer->Disconnect();
	localPeer->ShutdownNetworking();
	delete localPeer;
	localPeer = nullptr;
	mConnected = false;
	return shutdown;
}

// these read functions will call server functions to read data and deal with it there
// for testing purposes they are only writing to a file rn
int readDataInt(int data, char * varName, float unityTime)
{
	// call the general read function because it's easier 
	// than writing 10 functions that do the same thing

	return localPeer->WriteData(data, 0, varName, unityTime);

}

void readDataFloat(float data, char * varName, float unityTime)
{
	// call the general read function because it's easier 
	// than writing 10 functions that do the same thing

	localPeer->WriteData(data, 2, varName, unityTime);
}

void readDataDouble(double data, char * varName, float unityTime)
{
	// call the general read function because it's easier 
	// than writing 10 functions that do the same thing

	localPeer->WriteData(data, 1, varName, unityTime);
}

void readDataChar(char data, char * varName, float unityTime)
{
	// call the general read function because it's easier 
	// than writing 10 functions that do the same thing

	localPeer->WriteData(data, 3, varName, unityTime);
}

void readDataCharP(char * data, char * varName, float unityTime)
{
	localPeer->WriteData(data, 4, varName, unityTime);
}

void sendMessage(char * message, float unityTime)
{
	localPeer->SendChat(message, unityTime);
}

bool shouldQuit()
{
	if(mConnected)
		return localPeer->checkIsConnected();

	return false;
}
