/*
	EGP Networking: Plugin Configuration
	Dan Buckstein
	October 2018

	Utility for configuring library symbol exports and imports.
*/

#ifndef _EGP_NET_PLUGIN_CONFIG_H_
#define _EGP_NET_PLUGIN_CONFIG_H_

// this is a user defined plag found in C/C++->Preprocessor
// this lets the compiler know if it is importing, exporting, or neither
#ifdef EGP_NET_DLLEXPORT
// compiler logic for DLL-producing project
// e.g. the plugin itself

// if you are exporting set the tag to exportdll
#define DLLExport __declspec(dllexport)	// tmp linker flag, forces lib to exist

#else	// !EGP_NET_DLLEXPORT
#ifdef EGP_NET_DLLIMPORT
// compiler logic for DLL-consuming project
// e.g. Unity game; test app


// if you are importing set the tag to importdll
#define EGP_NET_SYMBOL __declspec(dllimport)	// tmp linker flag, forces lib to exist


#else	// !EGP_NET_DLLIMPORT

// if neither then it is a dummy tag
#define EGP_NET_SYMBOL

// compiler logic for DLL-unrelated project
// e.g. static code project

#endif	// EGP_NET_DLLIMPORT
#endif	// EGP_NET_DLLEXPORT


#endif	// !_EGP_NET_PLUGIN_CONFIG_H_