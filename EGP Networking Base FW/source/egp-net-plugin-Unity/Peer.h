/*
	Tyler Chapman and Alexander West
	EGP-405-01
	Final Project
	Due December 13

	I certify that this work is entirely my own. The assessor of this
	project may reproduce this project and provide copies to other academic staff,
	and/or communicate a copy of this project to a plagiarism-checking service,
	which may retain a copy of the project on its database
*/

#ifndef PLUGIN_PEER_H
#define PLUGIN_PEER_H

#include "egp-net/fw/egpNetPeerManager.h"
#include "egp-net/fw/PluginSerializableData.h"
#include "RakNet/StringCompressor.h"
#include <typeinfo>

class Peer : public egpNetPeerManager
{
	public:
		Peer();
		~Peer();

		enum me_ServerPacketIdentifier
		{
			e_id_serverPacketBegin = e_id_packetEnd,

			e_id_clientSendData,

			// end
			e_id_serverPacketEnd
		};
		
		int HandleNetworking() const;
		int SendPacket(const RakNet::BitStream *bs, const short connectionIndex, const bool broadcast, const bool reliable) const
		{
			return egpNetPeerManager::SendPacket(bs, connectionIndex, broadcast, reliable);
		};

		bool checkIsConnected()
		{
			return egpNetPeerManager::checkIsConnected();
		}

		static int WriteMessageID(RakNet::BitStream *bs, const char &messageID) { return egpNetPeerManager::WriteMessageID(bs, messageID); };
		static int ReadMessageID(RakNet::BitStream *bs, char &messageID_out) { return egpNetPeerManager::ReadMessageID(bs, messageID_out); };

		template <typename T>
		int WriteData(T data, int varType, char * name, float unityTime);
		void SendChat(char * message, float unityTime);
		bool getExitCondition() { return mShouldExit; };
	protected:
		//int ProcessAllPackets() const;
		int ProcessPacket(const RakNet::Packet *const packet, const unsigned int packetIndex) const;

	private:
		bool mShouldExit;
};

#endif

template<typename T>
inline int Peer::WriteData(T data, int varType, char * name, float unityTime)
{
	//std::string test = name;
	RakNet::BitStream bs;
	bs.Write((RakNet::MessageID)e_id_clientSendData);
	//RakNet::StringCompressor compTest;
	//RakNet::RakString *testString = new RakNet::RakString(name);
	//compTest.EncodeString(testString, nameLen, bs));
	//bs.Write(dataSize);

	// vartype decides what type is ent
	// 0 = int, 1 = double, 2 = float, 3 = char, 4 = string, 5 = char array

	bs.Write(varType);
	bs.Write(name);
	bs.Write(data);
	bs.Write(unityTime);
	

	//PluginSerializableData<T> * serializableData = new PluginSerializableData<T>(data, dataSize, nameLen, name, (char*)getIP().c_str());

	//serializableData->Serialize(&bs);

	return SendPacket(&bs, 0, 0, 1);
}

inline void Peer::SendChat(char * message, float unityTime)
{
	RakNet::BitStream bs;
	bs.Write((RakNet::MessageID)e_id_messageTransfer);

	bs.Write(message);
	bs.Write(unityTime);

	SendPacket(&bs, 0, 0, 1);
}