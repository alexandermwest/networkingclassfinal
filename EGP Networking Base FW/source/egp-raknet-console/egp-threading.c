// custom thread

#include "egp-thread.h"

#include <Windows.h>


// testing only
#include <stdio.h>

//--------------------------------------------

long __stdcall egpThreadLauncherInternal(egpThread *param)
{
	param->running = 1;
	param->result = param->func(param->args);
	param->running = 0;
	// tests
	// while(1)
	//	printf("\t hello thread \t");

	return (param->result);
}

//--------------------------------------------


int egpThreadCreate(egpThread *thread_out, egpThreadFunc func, void *args)
{
	if (thread_out && !thread_out->handle && func)
	{
		// reset thread struct
		thread_out->result = 0;
		thread_out->running = 0;
		thread_out->func = func;
		thread_out->args = args;

		// launch thread
		thread_out->handle = CreateThread(0, 0, egpThreadLauncherInternal , thread_out , 0, &thread_out->id);

		return (thread_out->id);
	}

	return 0;
}

int egpThreadTerminate(egpThread *thread)
{
	if (thread && thread->handle)
	{
		int success = TerminateThread(thread->handle, -1);
		if (success)
		{
			success = CloseHandle(thread->handle);
			if (success)
			{
				thread->handle = 0;
				return 1;
			}
		}


		return 1;
	}
	return 0;
}