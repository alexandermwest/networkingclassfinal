// custom thread wrapper

#ifndef EGP_THREAD_H
#define EGP_THREAD_H

#ifdef __cplusplus
extern "C"
{

#else // __cplusplus

// c++ struct decl
// struct foo {}...
// foo bar; // c++ instance
// struct foo bar; // c instance

typedef struct egpThread egpThread;

#endif // __cplusplus

// our definition of an acceptable function
// any function tha treturns int and accepts ptr param
typedef int(*egpThreadFunc)(void *);


// thread interface
struct egpThread
{
	void *handle;
	unsigned int id;

	int result;
	int running;

	egpThreadFunc func;
	void *args;
};

int egpThreadCreate(egpThread *thread_out, egpThreadFunc func, void *args);

int egpThreadTerminate(egpThread *thread);

#ifdef __cplusplus
}
#else // __cplusplus
#endif // __cplusplus

#endif