/*
	Tyler Chapman and Alexander West
	EGP-405-01
	Final Project
	Due December 13

	I certify that this work is entirely my own. The assessor of this
	project may reproduce this project and provide copies to other academic staff,
	and/or communicate a copy of this project to a plagiarism-checking service,
	which may retain a copy of the project on its database
*/

#ifndef SERVER_PEER_H
#define SERVER_PEER_H

#include "egp-net/fw/egpNetPeerManager.h"
#include <iostream>

const std::string GLOBAL_DIR_PATH = "..\\..\\..\\..\\logs";

class ServerPeer : public egpNetPeerManager
{
	public:
		ServerPeer();
		~ServerPeer();
	
		enum me_ServerPacketIdentifier
		{
			e_id_serverPacketBegin = e_id_packetEnd,
			
			e_id_serverReceiveData,
	
			// end
			e_id_serverPacketEnd
		};
	
		int HandleNetworking() const;
		int SendPacket(const RakNet::BitStream *bs, const short connectionIndex, const bool broadcast, const bool reliable) const
		{
			return egpNetPeerManager::SendPacket(bs, connectionIndex, broadcast, reliable);
		};
		static int WriteMessageID(RakNet::BitStream *bs, const char &messageID) { return egpNetPeerManager::WriteMessageID(bs, messageID); };
		static int ReadMessageID(RakNet::BitStream *bs, char &messageID_out) { return egpNetPeerManager::ReadMessageID(bs, messageID_out); };

		void killall()
		{
			egpNetPeerManager::killall();
		}

		int kill(int index)
		{
			return egpNetPeerManager::kill(index);
		}

		std::string printList() { std::cout << egpNetPeerManager::printList(); return ""; };
	protected:
		//int ProcessAllPackets() const;
		int ProcessPacket(const RakNet::Packet *const packet, const unsigned int packetIndex) const;

	private:
		std::string unityTimeToString(float &unityTime) const;
};
#endif