/*
	Tyler Chapman and Alexander West
	EGP-405-01
	Final Project
	Due December 13

	I certify that this work is entirely my own. The assessor of this
	project may reproduce this project and provide copies to other academic staff,
	and/or communicate a copy of this project to a plagiarism-checking service,
	which may retain a copy of the project on its database
*/

#include <iostream>
#include <windows.h>
#include <conio.h>
#include "Wincon.h"

#include "ServerPeer.h"

void handleInput(char* consoleMessage, ServerPeer* localPeer, int &messageSize)
{
	int key = 0;

	printf("\rMessage: [%*.*s]", 1, messageSize, consoleMessage);
	if (_kbhit())
	{
		//FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
		key = _getch();
		if (key == 8)
		{
			if (messageSize > 0)
			{
				messageSize--;
			}
			// mimics a delete event
			printf("\rMessage: [%*.*s]", 1, messageSize, consoleMessage);
			printf(" ");
		}
		else if (key != 0)
		{
			if (key == 13)
			{
				consoleMessage[int(messageSize)] = '\0';

				std::string stringMessage = consoleMessage;

				if (stringMessage.substr(0, 7) == "killall")
				{
					localPeer->killall();
				}
				else if (stringMessage.substr(0, 4) == "kill")
				{
					int errorCheck = localPeer->kill(stoi(stringMessage.substr(5, 6)));

					if (errorCheck != 0)
						printf("\rError: Invalid index sent. Try command list for valid indexes");
				}
				else if (stringMessage.substr(0, 4) == "list")
				{
					localPeer->printList();
				}
				else if (stringMessage.substr(0, 4) == "help")
				{
					printf("\nCommands:\n");
					printf("killall					 -- Disconnects all clients connected and closes their application \n");
					printf("kill <index>				 -- Disconnects the specified index and closes their application \n");
					printf("list					 -- Lists all connected users by index and IP \n");
					printf("help					 -- Prints this message \n");
				}
				else
				{
					printf("\nNot a recognized command. Type help for a list of commands \n");
				}
				printf("\n");
				messageSize = 0;
				consoleMessage[0] = '\0';
				memset(consoleMessage, 0, sizeof(consoleMessage));
			}
			else
			{
				consoleMessage[int(messageSize)] = key;
				messageSize++;
				printf("\rMessage: [%*.*s]", 1, messageSize, consoleMessage);
			}
		}
	}
}

int main()
{
	// remove this to not clear all files
	std::string command = "del /Q  " + GLOBAL_DIR_PATH + "\\*.txt";
	system(command.c_str());
	ServerPeer* localServerPeer = new ServerPeer();
	localServerPeer->StartupNetworking(0, 10, 60000);
	char * consoleMessage = new char[512];
	int consoleSize = 0;
	// path to top directory
	

	// creates 
	if (GetFileAttributes(GLOBAL_DIR_PATH.c_str()) == INVALID_FILE_ATTRIBUTES)
	{
		// no dir exists
		CreateDirectory(GLOBAL_DIR_PATH.c_str(), NULL);
	}
	

	while (1/*shouldExit*/)
	{
		localServerPeer->HandleNetworking();

		handleInput(consoleMessage, localServerPeer, consoleSize);
		
	}
	localServerPeer->ShutdownNetworking();
	delete localServerPeer;
	localServerPeer = nullptr;
	delete[] consoleMessage;
	consoleMessage = nullptr;
	return 0;
}