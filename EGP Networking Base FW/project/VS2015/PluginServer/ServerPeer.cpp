/*
	Tyler Chapman and Alexander West
	EGP-405-01
	Final Project
	Due December 13

	I certify that this work is entirely my own. The assessor of this
	project may reproduce this project and provide copies to other academic staff,
	and/or communicate a copy of this project to a plagiarism-checking service,
	which may retain a copy of the project on its database
*/

#include "RakNet/RakString.h"
#include "RakNet/GetTime.h"
#include "RakNet/gettimeofday.h"
#include "ServerPeer.h"

#include <typeinfo>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>


ServerPeer::ServerPeer()
{
}

ServerPeer::~ServerPeer()
{
}

int ServerPeer::HandleNetworking() const
{
	if (!egpNetPeerManager::HandleNetworking())
	{

	}
	return 0;
}

int ServerPeer::ProcessPacket(const RakNet::Packet * const packet, const unsigned int packetIndex) const
{
	egpNetPeerManager::ProcessPacket(packet, packetIndex);
	switch (packet->data[0])
	{
		// specific packet logic
		case ID_DISCONNECTION_NOTIFICATION:
		{
			printf("\rA client has disconnected.\n");
			break;
		}
		case ID_CONNECTION_LOST:
		{
			printf("\rA client has lost the connection.\n");
			break;
		}
		case e_id_serverReceiveData:
		{
			int typeInfo = 0;
			float unityTime;

			std::string fileName = packet->systemAddress.ToString(false);
			fileName += ".txt";

			std::ofstream oStream;
			oStream.open(GLOBAL_DIR_PATH + "\\" + fileName, std::fstream::app);

			RakNet::RakString rs;
			RakNet::BitStream bs(packet->data, packet->length, false);
			bs.IgnoreBytes(sizeof(RakNet::MessageID));
			//rs.DeserializeCompressed(bs, false);
			 //rs.DeserializeCompressed(bs, false);

			bs.Read(typeInfo);
			bs.Read(rs);
			
			oStream << "Name: " << std::left << std::setw(25) << rs.C_String() << "Data: " << std::left << std::setw(25);
			switch (typeInfo)
			{
				case 0:
				{
					int passedInt;
					bs.Read(passedInt);
					oStream << passedInt;
					break;
				}
				case 1:
				{
					double passedDouble;
					bs.Read(passedDouble);
					oStream << passedDouble;
					break;
				}
				case 2:
				{
					float passedFloat;
					bs.Read(passedFloat);
					oStream << passedFloat;
					break;
				}
				case 3:
				{
					char passedChar;
					bs.Read(passedChar);
					oStream << passedChar;
					break;
				}
				case 4:
				{
					RakNet::RakString passedString;
					bs.Read(passedString);
					oStream << passedString.C_String();
					break;
				}
				default:
				{
					break;
				}
			}
			bs.Read(unityTime);
			std::string unityTimeString = unityTimeToString(unityTime);
			unityTime = fmod(unityTime, 3600);
			unityTime = fmod(unityTime, 60);
			oStream << "Time: " << unityTimeString << ((float)((int)(unityTime * 100))) / 100 << std::endl;
			break;
		}
		case e_id_messageTransfer:
		{
			std::ofstream chatStream;
			RakNet::RakString rs;
			float unityTime;

			chatStream.open(GLOBAL_DIR_PATH + "\\" + "ErrorFile.txt", std::fstream::app);
			RakNet::BitStream bs(packet->data, packet->length, false);

			bs.IgnoreBytes(sizeof(RakNet::MessageID));
			bs.Read(rs);
			bs.Read(unityTime);
			std::string unityTimeString = unityTimeToString(unityTime);
			unityTime = fmod(unityTime, 3600);
			unityTime = fmod(unityTime, 60);

			std::cout << "\rUser: " << packet->systemAddress.ToString(false) << " sent: " << rs.C_String() << " at time: " << unityTimeString << ((float)((int)(unityTime * 100))) / 100 << std::endl;

			chatStream << "\rUser: " << packet->systemAddress.ToString(false) << " sent: " << rs.C_String() << " at time: " << unityTimeString << ((float)((int)(unityTime * 100))) / 100 << std::endl;
			break;
		}
		default:
		{
			//printf("Received packet with ID" + static_cast<int>(packet->data[0]));
			//std::cout << "Receieved packet with ID" << static_cast<int>(packet->data[0]) << std::endl;
			break;
		}
	}
	return 0;
}

std::string ServerPeer::unityTimeToString(float &unityTime) const
{
	int hour = unityTime / 3600;
	unityTime = fmod(unityTime, 3600);
	int min = unityTime / 60;
	unityTime = fmod(unityTime, 60);
	return std::to_string(hour) + ":" + std::to_string(min) + ":";
}
