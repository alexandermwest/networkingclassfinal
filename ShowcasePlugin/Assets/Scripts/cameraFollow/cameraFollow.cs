﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour {
	/* PUBLIC VARIABLES */

	/* VECTORS */
	// zero's out the gameObjects velocity
	private Vector3 velocity = Vector3.zero;

	// offset from the origin of the other gameObject
	public Vector3 offset;

	/* TRANSFORMS */
	// position of player
	public Transform player;

	/* FLOATS */
	// value that the gameObject can't go under/ove (Depending how it's used)
	public float yMin = 0.0f;

	// crank this up to 0.000001 or lower for best effect
	public float speedSmoother = 0.125f;


	/* PRIVATE VARIABLES*/

	/* FLOATS */
	// fixed rotation point
	float zPosition;
	
	// Update is called once per frame
	void LateUpdate () {
		// assign the origin rotation
		zPosition = transform.position.z;

		// desried position to follow + offset
		Vector3 desiredPosition = player.position + offset;

		// smoothed position 
		Vector3 smoothedPosition = Vector3.SmoothDamp (transform.position, desiredPosition, ref velocity, speedSmoother);

		// change position
		transform.position = smoothedPosition;

		// applies the changes but changes the z component back to what it was originally
		transform.position = new Vector3(transform.position.x, transform.position.y, zPosition);

		// checks bounds for corrections
		checkBounds ();
	}

	void checkBounds()
	{
		// if it is the floor...
		if (gameObject.name == "floorObj") {
			// don't move the floor at all essentially
			if (transform.position.y >= yMin || transform.position.y <= yMin) {
				transform.position = new Vector3 (transform.position.x, yMin, transform.position.z);
			}
		}
		// anything else set the y position to its yMin if it goes under or equal to the yMin
		else if (transform.position.y <= yMin) {
			transform.position = new Vector3 (transform.position.x, yMin, transform.position.z);
		} 
	}
}
