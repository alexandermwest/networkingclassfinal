﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour {
	/* PUBLIC VARIABLES */

	/* FLOAT */
	// force to add 
	public float spinmore = 1f;
	public float spinSpeed = 0.0f;
	public float startingVelocity;
	public float degree;

    // variables for plugin test
    public float xVelocity = 0.0f;
    public float yVelocity = 0.0f;

	/* PRIVATE VARIABLES */

	/* GAMEOBJECTS */
	GameObject scope;
	GameObject slider;

	/* RIGIDBODY */
	Rigidbody2D rd;

	/* FLOATS */
	// initial rotating angle
	float degrees = 90.0f;

	// timer to slow player over time when on ground
	float timer = 0f;

	// time necessary for penguin to be on the ground to start slowing down
	float stop = 0.1f;

	// initial starting Velocity for penguin when launched
	float startVelocity;

	/* BOOLS */
	// to slow penguin gradually over time
	bool firstTime = true;

	// Use this for initialization
	void Start () {
		//fillArea = GameObject.Find ("Canvas/Fill Area");
		slider = GameObject.Find ("Canvas/Slider");
		scope = GameObject.Find ("scope");

		rd = GetComponent<Rigidbody2D> ();
		rd.gravityScale = 0;

		spinSpeed = 0.0f;

		scope.GetComponent<SpriteRenderer> ().color = new Color (0.0f, 0.0f, 0.0f, 0.0f);
		gameObject.GetComponent<CircleCollider2D> ().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        xVelocity = rd.velocity.x;
        yVelocity = rd.velocity.y;
        // re-enable collider before it is hit (disabled so it doesn't hit interactablles while being pitched)
        if (transform.position.x < 3.81f) {
			gameObject.GetComponent<CircleCollider2D> ().enabled = true;
		}
		transform.Rotate (0,0,degrees*spinSpeed*Time.deltaTime); 

		// if the player is on the ground...
		if (gameObject.transform.position.y < -2.9f) {
			timer += Time.deltaTime;
		} else {
			timer = 0f;
		}

		// stops player slowly over time when it is rolling on the ground
		if (timer > stop) {
			rd.velocity = rd.velocity * 0.9f;
			timer = 0f;
		}

		// stops player completely at a point
		if (slider.GetComponent<PowerControl>().fired == true) {
			// slow the penguin at a slower rate (looks smoother)
			if (firstTime) {
				StartCoroutine (waitTime ());
			} else {
				if (rd.velocity.x < 2f) {
					rd.velocity = new Vector2 (0, 0);
				}
			}
		}
	}

	void OnCollisionEnter2D(Collision2D col) 
	{
		// when it hits the  ground set the spinning to whatever happens with physics
		spinSpeed = 0.0f;
	}
		
	public void fire(int percentShot, float degree)
	{
		// show the scope
		scope.GetComponent<SpriteRenderer> ().color = new Color (1.0f, 1.0f, 1.0f, 1.0f);
		spinSpeed = 1.0f;
		rd.gravityScale = 1;

		// if the swing power is lower than 60, set it to 60
		if (percentShot > 60) {
			startVelocity = (startingVelocity / 10) * (percentShot / 10);
		} else {
			startVelocity = (startingVelocity / 10) * (60 / 10);
		}

		// add force in a specific angle
		rd.velocity = new Vector3 (Mathf.Cos (Mathf.Deg2Rad * degree), Mathf.Sin (Mathf.Deg2Rad * degree), 0f) * (startVelocity);
	}

	public void addMoreForce(float angleToUse, float forceToUse)
	{
		// if you are falling do not add that y component of velocity to the new directional velocity
		if (rd.velocity.y < 0) {
			rd.velocity = new Vector2 (rd.velocity.x, rd.velocity.y*0.0f);
			startVelocity = rd.velocity.magnitude;
			rd.velocity = new Vector3 (Mathf.Cos (Mathf.Deg2Rad * angleToUse), Mathf.Sin (Mathf.Deg2Rad * angleToUse), 0f) * (startVelocity);
		} else {
			rd.velocity = new Vector2 (rd.velocity.x, rd.velocity.y);
			startVelocity = rd.velocity.magnitude;
			rd.velocity = new Vector3 (Mathf.Cos (Mathf.Deg2Rad * angleToUse), Mathf.Sin (Mathf.Deg2Rad * angleToUse), 0f) * (startVelocity);
		}

		// add force at the angle specified
		Vector3 dir = Quaternion.AngleAxis (angleToUse, Vector3.forward) * Vector3.right;
		rd.AddForce (dir * forceToUse);

		rd.gravityScale = 1;
	}

	IEnumerator waitTime()
	{
		yield return new WaitForSeconds (0.5f);
		if (rd.velocity.x < 2f) {
			rd.velocity = new Vector2 (0, 0);
		}
		firstTime = false;
	}
}