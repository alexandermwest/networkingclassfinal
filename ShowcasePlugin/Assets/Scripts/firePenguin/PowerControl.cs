﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerControl : MonoBehaviour
{
	/* PUBLIC VARIABLES */

	/* AUDIO CLIPS */
	// song to play before hit
	public AudioClip beginSong;

	// song to play while playing
	public AudioClip playSong;

	/* SLIDERS */
	// shop slider
	public Slider shopSlider;

	/* TEXT */
	// UI text for player shop hold feedback
	public Text spaceReleeaseText;

	/* GAMEOBJECTS */
	// Stuff for detecting different space inputs
	public GameObject gameManager;

	// player
	public GameObject player;

	// baseball
	public GameObject baseball;

	/* INTS */
	// speed of pitch
	public int pitchForce = 3000;

	// speed of slider going up
	public int upSpeed = 5;

	// speed of slider going down
	public int downSpeed = 3;

	/* BOOLS */
	// if slider has stopped
	public bool stop = false;

	// if penguin has been hit
	public bool fired = false;

	// if the power slider is active
	public bool sliderActive;

	// if the shop is in use
	public bool useShop = false;


	/* PRIVATE VARIABLES */

	/* SLIDERS */
	// power slider 
    Slider slider;

	/* GAMEOBJECTS */
	// fill area for power slider
	GameObject fillArea;

	/* INTS */
	// set in constructor
	int maxHeight;
	int minHeight;

	// Penguin spawn height
	int xSpawnPos;
	int ySpawnPos;

	/* FLOATS */
	// time first button is pressed
	float timeOfFirstButton;

	// delay for it to be a hold
	float heldDelay = 0.13f;

	// time currently held before on key up
	float heldTime = 0.0f;

	/* BOOLS */

	// true if power slider moving up, false if down
	bool upOrDown = true; 

	// if penguin is pitched
	bool pitched = false;

	// if the shop has been usd
	bool shopUsed = false;

	// if key is being held
	bool holding = false;

	// is bat collider active
	bool batActive;

	/* REFERENCES */
	// reference to cameraFollowScript
	cameraFollow followScript;

    void Start ()
    {
		// play beginning song
		SoundManager.instance.changeMusic (beginSong);

		// find gameObject
		fillArea = GameObject.Find ("Canvas/Fill Area");

		// find gameObject
        slider = gameObject.GetComponent<Slider>();

		// assign max and min values for slider
        maxHeight = (int)slider.maxValue;
        minHeight = (int)slider.minValue;

        // set penguin spawn location
        xSpawnPos = (int)Camera.main.ViewportToWorldPoint(new Vector3(1.0f, 0, 0.0f)).x;
        ySpawnPos = (int)Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 1.0f, 0.0f)).y;

		// make slider move
        sliderActive = true;

		// make bat collider deactivated
        batActive = false;

		// deactivate the camera follow
        followScript = Camera.main.GetComponent<cameraFollow>();
        followScript.enabled = false;

		// spawn penguin at origin coordinates
		SpawnPenguin();
    }

    void Update()
    {
		// if shop has been used disabled the shop slider
        if (shopUsed)
        {
            shopSlider.gameObject.SetActive(false);
        }

       // if slider has not stopped then charge slider
        if (!stop) {
            Charge();
        }
		// if holding and slider is not active then space is being held
        if (holding && sliderActive == false) {
            heldTime += Time.deltaTime;

			// if time being held is less than time ot open shop than increase slider % fill
            if (!(heldTime >= 1.5f))
            {
				// hard coded the 4.7f.. Works most of the time
				// don't make it too high where the text appears too quick
				// or too low where bar does not fill
				shopSlider.value += Mathf.Round((heldTime / 1.5f) * 4.7f);

                if (shopSlider.value >= shopSlider.maxValue)
                {
					//Debug.Log (heldTime);

					// show text if it has been held for 1.5 seconds
                    spaceReleeaseText.gameObject.SetActive(true);
                }
            }
        }

		if (Input.GetKeyDown (KeyCode.Space)) {
			// read input only if shop is not open
			if (gameManager.GetComponent<gameManager> ().shop == false) {
				holding = true;
			}
			// if shop was not used
			if (useShop == false) {
				// slider activate and no shop up
				if (sliderActive && gameManager.GetComponent<gameManager> ().shop == false) {
					// stop slider motion and set bat to active
					sliderActive = false;
					stop = true;
					batActive = true;
					//Pitch ();
				} else if (!pitched && gameManager.GetComponent<gameManager> ().shop == false) {
					// pitch
					pitched = true;
					Pitch ();
				}
				else if (batActive && gameManager.GetComponent<gameManager>().shop == false)
				{
					//Debug.Log("<color='red'>SPACE</color>");

					// set the pitch trigger and check collision
					baseball.GetComponent<Animator>().SetTrigger("Pitch");

					StartCoroutine(CheckCollision());
				}
			}
		}
		if (Input.GetKeyUp (KeyCode.Space)) {
			holding = false;
			// if shop is not used and hold time is greater than time to hold to open shop
			if (heldTime > heldDelay && !shopUsed) {
				if (heldTime > 1.5f) {
					// set sliders to deactivated
                    shopSlider.gameObject.SetActive(false);
                    spaceReleeaseText.gameObject.SetActive(false);
					useShop = true;
					heldTime = 0f;
					//Debug.Log ("HeldNnuff");
					gameManager.GetComponent<gameManager> ().heldEnough = true;
				}
			} else {
				// one time use to open shop, so if not held enough, still set shopUsed to true
				shopUsed = true;
			}

			// this could be causing bugs??
			if(gameManager.GetComponent<gameManager> ().shop == false)
			{
				useShop = false;
			}
			heldTime = 0.0f;
			//Debug.Log ("Unheld");
		}
    }

    void SpawnPenguin ()
    {
		// initial penguin spawn
        player.transform.position = new Vector3(xSpawnPos * 2.0f, ySpawnPos * -0.3f, -0.7600002f);
        // Debug.Log(player.transform.position);
    }

    void Pitch ()
    {
		// pitch penguin with pitchforce to left
		player.GetComponent<Rigidbody2D>().AddForce(Vector2.left * pitchForce);
    }

    IEnumerator CheckCollision()
    {
		// check the collision
        batActive = false;
        baseball.GetComponent<Animator>().SetTrigger("Pitch");

		// after delay, enable camera follow and set sliders off
        yield return new WaitForSeconds(0.5f);
		followScript.enabled = true;
		fired = true;
		gameObject.SetActive (false); 
		fillArea.SetActive (false);
        
		//groundObj.GetComponent<cameraFollow> ().enabled = true;
    }

    void Charge ()
    {
        if (upOrDown)
        {
            // Moving up
            slider.value += upSpeed;

            if (slider.value == maxHeight)
            {
                upOrDown = false;
            }
        }
        else // Moving Down
        {
            slider.value -= downSpeed;

            if (slider.value == minHeight)
            {
                upOrDown = true;
            }
        }
    }
}