﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Fire : MonoBehaviour {
	/* PUBLIC VAIRBALES */

	/* SOUNDS */
	public AudioClip clip;
	public AudioClip musicSwitch;

	/* SLIDERS */
	public Slider slider;

	/* GAMEOBJECTS */
	public GameObject player;
	public GameObject baseball;


	/* PRIVATE VARIABLES */

	/* REFERENCES */
	// reference to cameraFollowScript
	cameraFollow followScript;

	/* GAMEOBJECTS */
	// player feedback for swing text
	GameObject earlyOrLateText;

	/* ANIMATORS */
	// baseball animator 
	Animator baseballAnim;

	// Use this for initialization
	void Start () {
		// find gameObject
		earlyOrLateText = GameObject.Find ("Canvas/earlyOrLate");

		// initially deactivate the camera following the player
		followScript = Camera.main.GetComponent<cameraFollow>();
		followScript.enabled = false;

		// get the baseball animator component
		baseballAnim = baseball.gameObject.GetComponent<Animator>();

		//Debug.Log(baseballAnim.GetCurrentAnimatorStateInfo(0).normalizedTime);
	}

	// Update is called once per frame
	void Update () {
		// make sure the baseball collider is a trigger
		baseball.GetComponent<BoxCollider2D>().isTrigger = true;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		// current frame % in decimal form
		//Debug.Log(baseballAnim.GetCurrentAnimatorStateInfo(0).normalizedTime);

		// if it is early in the swing
		if (baseballAnim.GetCurrentAnimatorStateInfo (0).normalizedTime <= 0.39) {
			//Debug.Log("Too Early!");

			// disable the camera follow
			followScript = Camera.main.GetComponent<cameraFollow> ();
			followScript.enabled = false;

			// reset the camera position to original spot
			Camera.main.transform.position = new Vector3 (4.39f, 0f, -10.76f);

			// show the player the feedback
			earlyOrLateText.GetComponent<Text> ().text = "Swing Later...";

			// restart round
			StartCoroutine (restartRound ());
		} else if (baseballAnim.GetCurrentAnimatorStateInfo (0).normalizedTime > 0.39 && baseballAnim.GetCurrentAnimatorStateInfo (0).normalizedTime <= 0.5625) {
			// hit at lowest angle

			// play bat sound and start game music
			SoundManager.instance.playSingle (clip);
			SoundManager.instance.changeMusic (musicSwitch);

			// add force to the penguin based on slider value and angle 
			player.GetComponent<test> ().fire ((int)slider.value, 15.0f);

			// make the player a collider again
			player.GetComponent<CircleCollider2D> ().isTrigger = false;

			// disable the baseball collider to not get a false hit
			gameObject.GetComponent<BoxCollider2D> ().enabled = false;
		} else if (baseballAnim.GetCurrentAnimatorStateInfo (0).normalizedTime > 0.5625 && baseballAnim.GetCurrentAnimatorStateInfo (0).normalizedTime <= 0.625) {
			// hit at perfect angle

			// play bat sound and start game music
			SoundManager.instance.playSingle (clip);
			SoundManager.instance.changeMusic (musicSwitch);

			// add force to the penguin based on slider value and angle
			player.GetComponent<test> ().fire ((int)slider.value, 45.0f);

			// make the player a collider again
			player.GetComponent<CircleCollider2D> ().isTrigger = false;

			// disable the baseball collider to not get a false hit
			gameObject.GetComponent<BoxCollider2D> ().enabled = false;
		} else if (baseballAnim.GetCurrentAnimatorStateInfo (0).normalizedTime > 0.625 && baseballAnim.GetCurrentAnimatorStateInfo (0).normalizedTime <= 0.98) {
			// hit at highest angle

			// play bat sound and start game music
			SoundManager.instance.playSingle (clip);
			SoundManager.instance.changeMusic (musicSwitch);

			// add force to the penguin based on slider value and angle
			player.GetComponent<test> ().fire ((int)slider.value, 70.0f);

			// make the player a collider again
			player.GetComponent<CircleCollider2D> ().isTrigger = false;

			// disable the baseball collider to not get a false hit
			gameObject.GetComponent<BoxCollider2D> ().enabled = false;
		} else if (baseballAnim.GetCurrentAnimatorStateInfo (0).normalizedTime > 0.98) {
			// player swung too late
			//Debug.Log ("Too Late!");

			// turn off camera follow script
			followScript = Camera.main.GetComponent<cameraFollow> ();
			followScript.enabled = false;	

			// reset camera position to original position
			Camera.main.transform.position = new Vector3 (4.39f, 0f, -10.76f);

			// give player text feedback
			earlyOrLateText.GetComponent<Text> ().text = "Swing Earlier...";

			// restart the round
			StartCoroutine (restartRound ());
		} else {
			// to pick up if the player swings before the penguin reaches the box collider

			// disabled camera follow script
			followScript = Camera.main.GetComponent<cameraFollow> ();
			followScript.enabled = false;

			// reset camera position to original position
			Camera.main.transform.position = new Vector3 (4.39f, 0f, -10.76f);

			// give player text feedback
			earlyOrLateText.GetComponent<Text> ().text = "Swing Later...";

			// restart round
			StartCoroutine (restartRound ());
		}
	}

	IEnumerator restartRound()
	{
		// wait 1.5 seconds
		yield return new WaitForSeconds(1.5f);

		// show player 'try again' feedback
		earlyOrLateText.GetComponent<Text> ().text = "Try Again!";

		// wait .75 seconds to load new round
		yield return new WaitForSeconds(.75f);

		// load new round
		SceneManager.LoadScene ("testScene");
	}
}
