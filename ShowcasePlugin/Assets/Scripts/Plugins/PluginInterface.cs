﻿/*
	Tyler Chapman and Alexander West
	EGP-405-01
	Final Project
	Due December 13

	I certify that this work is entirely my own. The assessor of this
	project may reproduce this project and provide copies to other academic staff,
	and/or communicate a copy of this project to a plagiarism-checking service,
	which may retain a copy of the project on its database
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;

public class PluginInterface : MonoBehaviour
{
    [Header("Plugin Objects and Variables")]
    [SerializeField]
    private GameObjectDictionary gameObjectDictionary;
    [SerializeField]
    private ObjectDictionary monobehaviourDictionary;

    private ComponentFieldInfoDictionary componentVarDictionary;
    private FieldInfoConstraintDictionary fieldConstraintDictionary;
    

    // Use this for initialization
    void Start()
    {
        componentVarDictionary = new ComponentFieldInfoDictionary();
        fieldConstraintDictionary = new FieldInfoConstraintDictionary();
        checkGameObjects();
        checkScripts();
    }

    // Update is called once per frame
    void Update()
    {
    }

    [ContextMenu("Check Objects")]
    void checkGameObjects()
    {
        foreach(KeyValuePair<GameObject, AbstractList> pair in gameObjectDictionary)
        {
            List<ListEntry> variables = new List<ListEntry>(pair.Value.listEntries);
            GameObject gameObjectVar = pair.Key;
            foreach(Component component in gameObjectVar.GetComponents<Component>())
            {
                foreach(FieldInfo varInfo in component.GetType().GetFields())
                {
                    for (int i = 0; i < variables.Count; ++i)
                    {
                        try
                        {
                            //Debug.Log("Component Name: " + component.name + "\nVariable Name: " + propertyInfo.Name + "\nType: " + propertyInfo.PropertyType + "\nValue: " + propertyInfo.GetValue(component, null) + "\n");
                            if (variables[i].varName.Equals(varInfo.Name))
                            {
                                
                                if (!componentVarDictionary.ContainsKey(component))
                                {
                                    componentVarDictionary[component] = new FieldInfoListClass();
                                }
                                componentVarDictionary[component].fieldInfos.Add(varInfo);

                                if(!fieldConstraintDictionary.ContainsKey(varInfo))
                                {
                                    fieldConstraintDictionary[varInfo] = new Pair<float, float>(variables[i].average, variables[i].constraint);
                                }
                                // I wonder if this is faster or if it's slower
                                variables.RemoveAt(i);
                                i = variables.Count;
                            }
                        }
                        catch (Exception e)
                        {
                            //Debug.LogError(e);
                        }
                    }
                }
            }
        }
    }

    [ContextMenu("Check Scripts")]
    void checkScripts()
    {
        foreach (KeyValuePair<UnityEngine.Object, AbstractList> pair in monobehaviourDictionary)
        {
            List<ListEntry> variables = new List<ListEntry>(pair.Value.listEntries);
            UnityEngine.Object script = pair.Key;
            foreach (GameObject gObject in GameObject.FindObjectsOfType(typeof(GameObject)))
            {
                foreach (Component component in gObject.GetComponents<Component>())
                {
                    if (component.GetType().ToString() == script.name)
                    {
                        for (int i = 0; i < variables.Count; ++i)
                        {
                            foreach (FieldInfo varInfo in component.GetType().GetFields())
                            {
                                try
                                {
                                    if (variables[i].varName.Equals(varInfo.Name))
                                    {
                                        
                                        if (!componentVarDictionary.ContainsKey(component))
                                        {
                                            componentVarDictionary[component] = new FieldInfoListClass();
                                        }
                                        componentVarDictionary[component].fieldInfos.Add(varInfo);

                                        if (!fieldConstraintDictionary.ContainsKey(varInfo))
                                        {
                                            fieldConstraintDictionary[varInfo] = new Pair<float, float>(variables[i].average, variables[i].constraint);
                                        }
                                    }
                                    // I wonder if this is faster or if it's slower
                                    variables.Remove(variables[i]);
                                    i = variables.Count;
                                }
                                catch (Exception e)
                                {
                                    //Debug.LogError(e);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // Helper function to print the found variables
    void printFound()
    {
        foreach(KeyValuePair<Component, FieldInfoListClass> pair in componentVarDictionary)
        {
            Component component = pair.Key;
            foreach(FieldInfo varInfo in pair.Value.fieldInfos)
            {
                //Debug.Log("Found Name: " + varInfo.Name + "\nValue: " + varInfo.GetValue(component));
            }
        }
    }

    public ComponentFieldInfoDictionary getInfoPairs()
    {
        return componentVarDictionary;
    }

    public FieldInfoConstraintDictionary getConstraintPairs()
    {
        return fieldConstraintDictionary;
    }
}
