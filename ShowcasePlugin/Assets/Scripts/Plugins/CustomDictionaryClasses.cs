﻿/*
	Tyler Chapman and Alexander West
	EGP-405-01
	Final Project
	Due December 13

	I certify that this work is entirely my own. The assessor of this
	project may reproduce this project and provide copies to other academic staff,
	and/or communicate a copy of this project to a plagiarism-checking service,
	which may retain a copy of the project on its database
*/

// From documentation of SerializableDictionary from RotaryHeart

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using System.Reflection;

[System.Serializable]
public class Pair<T, U>
{
    public Pair(T first, U second)
    {
        this.first = first;
        this.second = second;
    }
    public T first { get; set; }
    public U second { get; set; }
}

[System.Serializable]
public class GameObjectDictionary : SerializableDictionaryBase<GameObject, AbstractList>
{

}

[System.Serializable]
public class ObjectDictionary : SerializableDictionaryBase<Object, AbstractList>
{

}

[System.Serializable]
public class ComponentFieldInfoDictionary : SerializableDictionaryBase<Component, FieldInfoListClass>
{

}

[System.Serializable]
public class FieldInfoConstraintDictionary : SerializableDictionaryBase<FieldInfo, Pair<float, float>>
{

}

[System.Serializable]
public class AbstractList
{
    public List<ListEntry> listEntries;

    public AbstractList()
    {
        listEntries = new List<ListEntry>();
    }
}

[System.Serializable]
public class ListEntry
{
    public string varName;
    public float average;
    public float constraint;
    public ListEntry()
    {
        varName = "";
        average = 0.0f;
        constraint = 0.0f;
    }
}

[System.Serializable]
public class FieldInfoListClass
{
    public List<FieldInfo> fieldInfos;

    public FieldInfoListClass()
    {
        fieldInfos = new List<FieldInfo>();
    }
}