﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;

public class TestScript : MonoBehaviour {

    public int testInt;
    public string testString;
    public float testFloat;
    public double testDouble;
    public char[] testArray;
    public char testChar;
    public List<char> testList;

	// Use this for initialization
	void Start ()
    {

	}

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(new string(testArray));
        if (Input.GetKeyDown(KeyCode.Return))
        {
            testInt++;
        }
	}
}
