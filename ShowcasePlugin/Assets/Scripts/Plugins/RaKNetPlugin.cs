﻿/*
	Tyler Chapman and Alexander West
	EGP-405-01
	Final Project
	Due December 13

	I certify that this work is entirely my own. The assessor of this
	project may reproduce this project and provide copies to other academic staff,
	and/or communicate a copy of this project to a plagiarism-checking service,
	which may retain a copy of the project on its database
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// need this!
using System.Runtime.InteropServices;
using System;
using System.Reflection;
using System.Text;

public class RaKNetPlugin : MonoBehaviour
{
    [DllImport("egp-net-plugin-Unity")]
    static extern int readDataInt(int data, byte[] name, float unityTime);
    [DllImport("egp-net-plugin-Unity")]
    static extern void readDataFloat(float data, byte[] name, float unityTime);
    [DllImport("egp-net-plugin-Unity")]
    static extern void readDataDouble(double data, byte[] name, float unityTime);
    [DllImport("egp-net-plugin-Unity")]
    static extern void readDataChar(char data, byte[] name, float unityTime);
    [DllImport("egp-net-plugin-Unity")]
    static extern void readDataCharP(byte[] data, byte[] name, float unityTime);
    [DllImport("egp-net-plugin-Unity")]
    static extern int startupPlugin(byte[] ipAdress);

    [DllImport("egp-net-plugin-Unity")]
    static extern int shutdownPlugin();

    [DllImport("egp-net-plugin-Unity")]
    static extern bool shouldQuit();

    GameObject plugin;

    [SerializeField]
    private const int timeDelay = 5;
    float currentTime = 0;

    public string serverIP;

    // DECLARE OTHER FUCNTIONS LIKE THE EXTERN INT FOO
    // ALSO NEED TO GO TO EGP-NET-PLUGINTEST AND DO TYPDEF WITH PARAM/TYPE
    // THEN DO THE GETPROCADDRESS
    // Use this for initialization
    void Start () {
        plugin = GameObject.Find("PluginObject");

        if(String.IsNullOrEmpty(serverIP))
        {
            Debug.Log("Error: IP needs to be specified. Enter it in the editor.");
        }
        else
        {
            int serverIPLength = serverIP.Length;
            byte[] IP = Encoding.UTF8.GetBytes(serverIP);

            //Debug.Log(new string(IP));
            // send server IP to plugin
            int errorCheck = startupPlugin(IP);

            // check valitidy of IP
            if (errorCheck != 1)
            {
                Debug.Log("Error: Issue with the IP entered for server. Make sure it is of standard length 16. Ex: 191.255.255.255");
            }
            else if(errorCheck == 1)
            {
                // connect to server with IP
                Debug.Log("Connected successfully!");
            }
            else
            {
                Debug.Log("Nope!");
            }

        }
    }

    // helper function to get 
    void getName(ref char[] nameArray, ref int nameSize, string nameString)
    {
        for (int x = 0; x < nameSize; x++)
        {
            nameArray[x] = nameString[x];
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if(shouldQuit())
        {
            //shutdownPlugin();
            #if UNITY_EDITOR
                    // Application.Quit() does not work in the editor so
                    // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
                    UnityEditor.EditorApplication.isPlaying = false;
                    //Debug.Log("IN HERE");
            #else
                    Application.Quit();
            #endif
        }

        // exit loop if no IP is specified
        if (String.IsNullOrEmpty(serverIP))
            return;

        // increment time counter to send data
        currentTime += Time.deltaTime;

        // check counter to frame send count
        if (currentTime > timeDelay)
        {
            
            // reset counter
            currentTime = 0;
            // get each component in the component dictionary in the plugin front-end script
            // send each typed variable found component to the plugin
            foreach (KeyValuePair<Component, FieldInfoListClass> pair in plugin.GetComponent<PluginInterface>().getInfoPairs())
            {
                Component component = pair.Key;
                foreach (FieldInfo varInfo in pair.Value.fieldInfos)
                {
                    if (varInfo.FieldType == typeof(string))
                    {
                        readDataCharP(Encoding.UTF8.GetBytes((string)varInfo.GetValue(component)), Encoding.UTF8.GetBytes(varInfo.Name), Time.realtimeSinceStartup);
                    }
                    else if (varInfo.FieldType == typeof(char[]))
                    {
                        readDataCharP(Encoding.UTF8.GetBytes((char[])varInfo.GetValue(component)), Encoding.UTF8.GetBytes(varInfo.Name), Time.realtimeSinceStartup);
                    }
                    else if (varInfo.FieldType == typeof(List<char>))
                    {
                        List<char> list = (List<char>)varInfo.GetValue(component);
                        readDataCharP(Encoding.UTF8.GetBytes(list.ToArray()), Encoding.UTF8.GetBytes(varInfo.Name), Time.realtimeSinceStartup);
                    }
                    else
                    {
                        FieldInfoConstraintDictionary infoConstraint = plugin.GetComponent<PluginInterface>().getConstraintPairs();
                        Pair<float, float> constraintPair = infoConstraint[varInfo];
                        if (varInfo.FieldType == typeof(int) 
                            && ((int)varInfo.GetValue(component) > constraintPair.first + constraintPair.second 
                            || (int)varInfo.GetValue(component) < constraintPair.first - constraintPair.second))
                        {
                            readDataInt((int)varInfo.GetValue(component), Encoding.UTF8.GetBytes(varInfo.Name), Time.realtimeSinceStartup);
                        }
                        else if (varInfo.FieldType == typeof(double)
                            && ((double)varInfo.GetValue(component) > constraintPair.first + constraintPair.second
                            || (double)varInfo.GetValue(component) < constraintPair.first - constraintPair.second))
                        {
                            readDataDouble((double)varInfo.GetValue(component), Encoding.UTF8.GetBytes(varInfo.Name), Time.realtimeSinceStartup);
                        }
                        else if (varInfo.FieldType == typeof(float)
                            && ((float)varInfo.GetValue(component) > constraintPair.first + constraintPair.second
                            || (float)varInfo.GetValue(component) < constraintPair.first - constraintPair.second))
                        {
                            readDataFloat((float)varInfo.GetValue(component), Encoding.UTF8.GetBytes(varInfo.Name), Time.realtimeSinceStartup);
                        }
                        else if (varInfo.FieldType == typeof(char)
                            && ((char)varInfo.GetValue(component) > constraintPair.first + constraintPair.second
                            || (char)varInfo.GetValue(component) < constraintPair.first - constraintPair.second))
                        {
                            readDataChar((char)varInfo.GetValue(component), Encoding.UTF8.GetBytes(varInfo.Name), Time.realtimeSinceStartup);
                        }
                    }
                }
            }
        }
    }

    private void OnApplicationQuit()
    {
        Debug.Log(shutdownPlugin());
        Debug.Log("Successfully disconnected");
    }
}
